package com.zuitt.example.Application.services;

import com.zuitt.example.Application.models.User;
import com.zuitt.example.Application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;
    public void createUser(User user){
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUserName(username));
    }
}
